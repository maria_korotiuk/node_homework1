const router = require('express').Router();
const fileController = require('../controllers/fileController');

router.post('/', fileController.createFile);

router.get('/', fileController.getFiles);

router.get('/:filename', fileController.getFile);

router.put('/:filename', fileController.updateFile);

router.delete('/:filename', fileController.deleteFile);

// router.use('/', (req, res) => {
//   res.status(400).send({
//     "message": "Bad request"
//   });
// });

module.exports = router;
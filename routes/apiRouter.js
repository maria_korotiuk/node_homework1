const router = require('express').Router();
const fileRouter = require('./fileRouter');

router.use('/files', fileRouter);

// router.use('/', (req, res) => {
//   res.status(400).send({
//     "message": "Bad request"
//   });
// })

module.exports = router;
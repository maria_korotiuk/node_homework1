const fs = require('fs');
const path = './files';

const checkExtension = (filename) => {
  const filenameArray = filename.split('.');
  const extension = filenameArray[filenameArray.length - 1];
  if (extension === 'log' || extension === 'txt' || extension === 'json' ||
    extension === 'yaml' || extension === 'xml' || extension === 'js') {
    return true;
  } else {
    return false;
  }
}

const checkFolder = (res) => {
  
}

module.exports = {
  getFiles(req, res) {
    res.set('Content-type', 'application/json');
    fs.access(path, (error) => {
      if (error) {
        fs.mkdir(path, (error) => {
          if (error) {
            res.status(500).send({
              "message": "Server error"
            });
            return;
          }
        });
      }
      fs.readdir(path, 'utf-8', (error, files) => {
        if (error) {
          res.status(500).send({
            "message": "Server error"
          });
        } else {
          res.status(200).send({
            "message": "Success",
            "files": files
          })
        }
      });
    });
  },

  async createFile(req, res) {
    const filename = req.body.filename;
    const content = req.body.content;
    const filepath = path + '/' + filename;
    res.set('Content-type', 'application/json');
    if (!filename) {
      res.status(400).send({
        "message": "Please specify 'filename' parameter"
      });
    } else if (!content) {
      res.status(400).send({
        "message": "Please specify 'content' parameter"
      });
    }  else if (!checkExtension(filename)) {
      res.status(400).send({
        "message": "Bad request"
      });
    } else {
      fs.access(path, (error) => {
        if (error) {
          fs.mkdir(path, (error) => {
            if (error) {
              res.status(500).send({
                "message": "Server error"
              });
              return;
            }
          });
        }
        fs.writeFile(filepath, content, 'utf-8', (error) => {
          if (error) {
            res.status(500).send({
              "message": "Server error"
            });
          } else {
            res.status(200).send({
              "message": "File created successfully"
            });
          }
        });
      });
    }
  },

  getFile(req, res) {
    const filename = req.params.filename;
    const filepath = path + '/' + filename;
    res.set('Content-type', 'application/json');
    fs.access(path, (error) => {
      if (error) {
        fs.mkdir(path, (error) => {
          if (error) {
            res.status(500).send({
              "message": "Server error"
            });
            return;
          }
        });
      }
      fs.access(filepath, (error) => {
        if (error) {
          res.status(400).send({
            "message": "Bad request"
          });
        } else {
          const filenameArray = filename.split('.');
          const extension = filenameArray[filenameArray.length - 1];
          fs.readFile(filepath, 'utf-8', (error, content) => {
            if (error) {
              res.status(500).send({
                "message": "Server error"
              });
            } else {
              fs.stat(filepath, (error, stats) => {
                if (error) {
                  res.status(500).send({
                    "message": "Server error"
                  });
                } else {
                  res.status(200).send({
                    "message": "Success",
                    "filename": filename,
                    "content": content,
                    "extension": extension,
                    "uploadedDate": stats.birthtime
                  });
                }
              });
            }
          });
        }
      });
    });
  },

  updateFile(req, res) {
    const filename = req.params.filename;
    const filepath = path + '/' + filename;
    res.set('Content-type', 'application/json');
    fs.access(path, (error) => {
      if (error) {
        fs.mkdir(path, (error) => {
          if (error) {
            res.status(500).send({
              "message": "Server error"
            });
            return;
          }
        });
      }
      fs.access(filepath, (error) => {
        if (error) {
          res.status(400).send({
            "message": "No file with '" + filename + "' filename found"
          });
        } else {
          const content = req.body.content;
          fs.writeFile(filepath, content, (error) => {
            if (error) {
              res.status(500).send({
                "message": "Server error"
              });
            } else {
              res.status(200).send({
                "message": "File updated successfully"
              });
            }
          });
        }
      });
    });
  },

  deleteFile(req, res) {
    const filename = req.params.filename;
    const filepath = path + '/' + filename;
    res.set('Content-type', 'application/json');
    fs.access(path, (error) => {
      if (error) {
        fs.mkdir(path, (error) => {
          if (error) {
            res.status(500).send({
              "message": "Server error"
            });
            return;
          }
        });
      }
      fs.access(filepath, (error) => {
        if (error) {
          res.status(400).send({
            "message": "No file with '" + filename + "' filename found"
          });
        } else {
          fs.rm(filepath, (error) => {
            if (error) {
              res.status(500).send({
                "message": "Server error"
              });
            } else {
              res.status(200).send({
                "message": "File deleted successfully"
              });
            }
          });
        }
      });
    });
  }
}
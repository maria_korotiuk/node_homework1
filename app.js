const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const apiRouter = require('./routes/apiRouter');
const app = express();
const port = 8080;

app.use(express.json());

app.use(morgan('tiny', { stream: fs.createWriteStream('./log.txt', { flags: 'a' })}));

app.use('/api', apiRouter);

// app.use('/', (req, res) => {
//   res.status(400).send({
//     "message": "Bad request"
//   });
// });

app.listen(port, console.log('Server is running'));